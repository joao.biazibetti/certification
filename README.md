This is the main repository for the Open Banking Brasil conformance suite, 
which covers Functional APIs published at https://openbanking-brasil.github.io/areadesenvolvedor/

The project is located at https://gitlab.com/obb1/certification/ 
and all issues / merge requests should be submitted there.

See the wiki for more info: https://gitlab.com/obb1/certification/-/wikis/home 

Note that this Conformance Suite shares its core code with the OpenID Foundation's
FAPI Conformance Suite, which is available at https://gitlab.com/openid/conformance-suite

Many operations are also shared, meaning those instructions will be interchangeable. 
https://gitlab.com/openid/conformance-suite/wikis/home

For user instructions for testing please see:
https://gitlab.com/obb1/certification/-/wikis/Instructions-for-running-Conformance-tests)

For user instructions for certification please see:
https://gitlab.com/obb1/certification/-/wikis/Certification-Guide

To get started as a developer or running the conformance suite locally, see:
https://gitlab.com/obb1/certification/-/wikis/Running-the-conformance-suite-locally

Additional tips may be seen at https://gitlab.com/openid/conformance-suite/wikis/Developers/Build-&-Run

Open Banking Brasil would like to thank the follow people/organisations:

* OpenID Foundation for open sourcing the FAPI conformance suite that forms
the basis of this code

* Raidiam, for developing and providing ongoing access to test environments compliant with
Brasil Functional APIs specifications.
