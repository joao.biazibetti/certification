package net.openid.conformance.openbanking_brasil.consent;

import net.openid.conformance.logging.ApiName;

/**
 * This class corresponds to {@link CreateNewConsentValidator}
 * Api url: https://github.com/OpenBanking-Brasil/areadesenvolvedor/blob/gh-pages/swagger/swagger_consents_apis.yaml
 * Api endpoint: /consents/{consentId}
 * Api git hash: 152a9f02d94d612b26dbfffb594640f719e96f70
 */
@ApiName("Consent Details Identified By ConsentId")
public class ConsentDetailsIdentifiedByConsentIdValidator extends CreateNewConsentValidator {
}
